module.exports = { handle };

async function handle(page, url) {
    //const data = [];
    let data = null;
    const items = await page.$$('.expansion-panel__container');
    for(let i = 0; i < items.length; i++) {
        const title = await getValueFromElement(items[i], '.expansion-panel__title');

        const date = title.split(" ")[0];
        //console.log("Date: "+date);

        //data.push(date);
        data = "last_date="+date;
    }
    return data;
}

const getValueFromElement = async (el, querySelector) => {
    const raw = await el.$eval(querySelector, name => name.textContent);
    return raw.replace("\n", "").trim();
}